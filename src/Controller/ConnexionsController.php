<?php

namespace App\Controller;

use App\Entity\Professor;
use App\Entity\Student;
use App\Entity\Users;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ConnexionsController extends AbstractController
{



    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils) {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        //
        $form = $this->get('form.factory')
            ->createNamedBuilder(null)
            ->add('_username', null, ['label' => 'Login'])
            ->add('_password', PasswordType::class,
                ['label' => 'Mot de passe'])
            ->add('ok', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, ['label' =>
                'Ok', 'attr' => ['class' => 'btn-primary btn-block']])
            ->getForm();


        return $this->render('connexions/index.html.twig', [
            'mainNavLogin' => true, 'title' => 'Connexion',
            //
            'form' => $form->createView(),
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }




    /**
     * @Route("/inscription", name= "inscription")
     */
    public function inscriptionAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $user = new Users();
        $form = $this->createFormBuilder($user)
            ->add('login', TextType::class, array('label' => "Login"))
            ->add('name', TextType::class, array('label' => "Name :"))
            ->add('lastname', TextType::class, array('label' => "Lastname :"))
            ->add('plainpassword', PasswordType::class, array('label' => "Pasword :"))
            ->add('type', ChoiceType::class, array("choices" => array("Student" => "student", "Professor" => "professor", "Admin" => "admin")  , "mapped" => false))
            ->add('save', SubmitType::class, array('label' => "Sign in"))
            ->getForm();
        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            # Before anything we check if the login is avaible
            $userRepo = $this->getDoctrine()->getRepository(Users::class);
            $isLoginAvaible = $userRepo->getUsersPerLogin($form->get('login')->getData());
            # If the login is not avaible we redirect to inscription page
            if(count($isLoginAvaible) > 0) {
                $this->addFlash('warning', "Sorry, this login is already used by someone else !");
                return $this->redirectToRoute('inscription');
            }

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            //on active par défaut
            $user->setIsActive(true);

            $userType = $form->get('type')->getData();


            if($userType == 'student'){
                $user->addRole("ROLE_STUDENT");
            }elseif($userType == 'professor'){
                $user->addRole("ROLE_PROFESSOR");
            }elseif ($userType == "admin"){
                $user->addRole("ROLE_ADMIN");
            } else{
                $user->addRole("ROLE_STUDENT");
            }
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
            $this->addFlash('success', 'Votre compte a bien été enregistré.');
            return $this->redirectToRoute('login');
        }
        return $this->render('connexions/inscription.html.twig', ['form' => $form->createView(),
            'mainNavRegistration' => true, 'title' => 'Inscription']);
    }
}
